﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour
{
    public Image healthBar;
    public Text scoreText;
    [SerializeField] Transform enemy;
    Animator anim;
    private int deathCount;
    public float startHealth = 100f;
    private float health;
    float timer = 0.0f; // begins at this value
    float timerMax = 3.0f; // event occurs at this value
    Vector3 start;

    public void ApplyDamage(float damage)
    {
        health -= damage;
        healthBar.fillAmount = health / startHealth;
        if (health <= 0)
        {
            anim.SetBool("isDead", true);

            enemy.transform.position = start;
            anim.SetBool("isDead", false);
            anim.SetBool("isWalking", true);
            health = startHealth;
            healthBar.fillAmount = health / startHealth;
            deathCount++;
            scoreText.text = "Score:" + deathCount;
        }
    }

    void Start()
    {
        anim = GetComponent<Animator>();
        //scoreText.text = "Score:" + deathCount;
        start = enemy.transform.position;
        health = startHealth;
    }

    private void Update()
    {
        timer += Time.deltaTime;
    }

}
