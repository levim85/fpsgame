﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AvatarHealth : MonoBehaviour
{

    public Slider healthBar;
    public string opponent;
    [SerializeField] Transform respawnPoint;
    [SerializeField] Transform player;
    private int deathCount;
    public Text scoreText;
    private float timer;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != opponent)
        {
            return;
        }
        healthBar.value -= 10;

        if (healthBar.value <= 0)
        {
            //MAGIC
            player.transform.position = respawnPoint.transform.position;
            healthBar.value = 100;
            deathCount++;
            scoreText.text = "Death:" + deathCount;
        }
    }

    void Start()
    {
        scoreText.text = "Death:" + deathCount;
    }
    void Update()
    {
        timer += Time.deltaTime;
    }

    void OnGUI()
    {
        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);
        string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);

        GUI.Label(new Rect(250, 10, 250, 100), niceTime);
    }
}
