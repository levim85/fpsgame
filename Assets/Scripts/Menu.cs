﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    // Use this for initialization
    public void PlayGame()
    {
        Debug.Log("START");
        SceneManager.LoadScene("Scene01");
    }

    // Update is called once per frame
    public void QuitGame()
    {
        Debug.Log("OVER");
        Application.Quit();

    }
}
