﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : MonoBehaviour
{
    public Text ammoText;
    private Animator anim;
    private AudioSource audioSource;
    public AudioClip shootSound;
    public float range = 100f;
    public int bulletsPerMag = 30;
    public int bulletsLeft = 200;
    public int currentBullets;
    public Transform shootPoint;
    private bool isReloading;
    public float fireRate = 0.1f;
    float fireTimer;
    public float damage = 20f;

    private bool shootIntput;
    public enum ShootMode { Auto, Semi }
    public ShootMode shootingMode;

    public GameObject hitParticles;
    public GameObject bulletImpact;

    public Vector3 originalPosition;
    public Vector3 aimPosition;

    public ParticleSystem muzzleFlash;
    public float aodSpeed = 8f;

    private void OnEnable()
    {
        UpdateAmmoText();
    }

    void Start()
    {
        anim = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
        currentBullets = bulletsPerMag;
        originalPosition = transform.localPosition;
        UpdateAmmoText();
    }

    void Update()
    {
        switch (shootingMode)
        {
            case ShootMode.Auto:
                shootIntput = Input.GetButton("Fire1");
                break;
            case ShootMode.Semi:
                shootIntput = Input.GetButtonDown("Fire1");
                break;

        }
        if (shootIntput)
        {
            if (currentBullets > 0)
                Fire();
            else if (bulletsLeft > 0)
            {
                DoReload();
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            if (currentBullets < bulletsPerMag && bulletsLeft > 0)
                DoReload();
        }

        if (fireTimer < fireRate)
        {
            fireTimer += Time.deltaTime;
        }
        AimDownSide();
    }

    private void DoReload()
    {
        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);
        if (isReloading) return;
        anim.CrossFadeInFixedTime("Reload", 0.01f);
    }

    private void FixedUpdate()
    {
        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);
        isReloading = info.IsName("Reload");
    }

    private void AimDownSide()
    {
        if (Input.GetButton("Fire2") && isReloading)
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, aimPosition, Time.deltaTime * aodSpeed );
        }
        else
        {
            transform.localPosition = Vector3.Lerp(transform.localPosition, originalPosition, Time.deltaTime * aodSpeed);
        }
    }

    private void Fire()
    {
        if (fireTimer < fireRate || currentBullets <= 0 || isReloading)
            return;

        RaycastHit hit;
        if (Physics.Raycast(shootPoint.position, shootPoint.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name + " found!");
            GameObject hitParticleEffect = Instantiate(hitParticles, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
            hitParticleEffect.transform.SetParent(hit.transform);
            GameObject bulletHole = Instantiate(bulletImpact, hit.point, Quaternion.FromToRotation(Vector3.forward, hit.normal));
            Destroy(hitParticleEffect, 1f);
            Destroy(bulletHole, 2f);

            if (hit.transform.GetComponent<HealthController>())
            {
                hit.transform.GetComponent<HealthController>().ApplyDamage(damage);
            }
        }
        //anim.SetBool("Fire", true);
        anim.CrossFadeInFixedTime("Fire", 0.01f);
        muzzleFlash.Play();
        //PlayShootSound();
        currentBullets--;
        UpdateAmmoText();
        fireTimer = 0.0f;
    }

    public void Reload()
    {
        if (bulletsLeft <= 0)
            return;

        int bulletsToLoad = bulletsPerMag - currentBullets;
        int bulletsToDeduct = bulletsLeft >= bulletsToLoad ? bulletsToLoad : bulletsLeft;

        bulletsLeft -= bulletsToDeduct;
        currentBullets += bulletsToDeduct;
        UpdateAmmoText();
    }

    private void PlayShootSound()
    {
        audioSource.PlayOneShot(shootSound);
        //audioSource.clip = shootSound;
        //audioSource.Play();
    }

    private void UpdateAmmoText()
    {
        ammoText.text = currentBullets+ " / " + bulletsLeft;
    }
}
